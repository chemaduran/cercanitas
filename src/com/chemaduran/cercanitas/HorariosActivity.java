/*
 * Cercanitas
 *
 * Chema Durán jgduran@gmail.com
 *
 */

package com.chemaduran.cercanitas;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;


public class HorariosActivity extends Activity{

    Thread t;
    ProgressDialog dialog;

    TextView info_transbordo;
    String fulldate;

    TableLayout tl;
    Vector<ParsedHorarioDataSet> test;

    String final_dia;
    String final_mes;
    String annio;

    // URL completa al script que parsea la web de renfe para obtener los horarios
    String parser_url = "http://horarios.renfe.com/cer/hjcer310.jsp";

    private static String TAG = "CHEMA_D";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.horario);

        tl = (TableLayout)findViewById(R.id.myTableLayout);

        final int nucleo_id = getIntent().getIntExtra("nucleo_id", 0);
        final int station1_id = getIntent().getIntExtra("station1_id", 0);
        final int station2_id = getIntent().getIntExtra("station2_id", 0);

        final String station1_label = getIntent().getStringExtra("station1_name");
        final String station2_label = getIntent().getStringExtra("station2_name");

        TextView info_stations = (TextView) findViewById(R.id.info_stations);
        TextView info_date = (TextView) findViewById(R.id.info_date);
        info_transbordo = (TextView) findViewById(R.id.info_transbordo);

        final_dia = getIntent().getStringExtra("day");
        final_mes = getIntent().getStringExtra("month");
        annio = getIntent().getStringExtra("year");

        Button change_btn = (Button) findViewById(R.id.btn_change);
        change_btn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), HorariosActivity.class);
                intent.putExtra("nucleo_id", nucleo_id);
                intent.putExtra("station1_id", station2_id);
                intent.putExtra("station2_id", station1_id);

                intent.putExtra("station1_name", station2_label);
                intent.putExtra("station2_name", station1_label);

                intent.putExtra("day", final_dia);
                intent.putExtra("month", final_mes);
                intent.putExtra("year", annio);

                startActivity(intent);

            }
        });

        info_stations.setText(station1_label + " -> " + station2_label);
        info_date.setText(final_dia + "/" + final_mes + "/" + annio);

        fulldate = annio + final_mes + final_dia;

        showDialog(0);
        t=new Thread() {
            public void run() {
                loadSchedule(nucleo_id, station1_id, station2_id, fulldate);
            }
        };
        t.start();

    }

    private StringBuilder inputStreamToString(InputStream is) throws IOException {
        String line = "";
        StringBuilder total = new StringBuilder();

        // Wrap a BufferedReader around the InputStream
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        // Read response until the end
        while ((line = rd.readLine()) != null) {
            total.append(line + "\r\n\r\n");
        }

        // Return full string
        return total;
    }

    private void EscribirAFichero (String str) {

        try
        {
            OutputStreamWriter fout=
                    new OutputStreamWriter(
                            openFileOutput("log.txt", Context.MODE_APPEND), "UTF-8");

            fout.write(str);
            fout.close();
        }
        catch (Exception ex)
        {
            Log.e("Ficheros", "Error al escribir fichero a memoria interna");
        }

    }

    private String stipHtml(String html) {
        return Html.fromHtml(html).toString();
    }

    public static int count(String word, String ch)  {

        int pos = word.indexOf(ch);
        return pos == -1 ? 0 : 1 + count(word.substring(pos+1),ch);
    }

    public void loadSchedule(int nucleo_id, int station1_id, int station2_id, String fulldate) {

        try {
            Log.d(TAG, "loadSchedule| URL: " + parser_url);

            // Create a new HttpClient and Post Header
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(parser_url);

            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("nucleo", String.valueOf(nucleo_id) ));
            nameValuePairs.add(new BasicNameValuePair("i", "s"));
            nameValuePairs.add(new BasicNameValuePair("I", "s"));
            nameValuePairs.add(new BasicNameValuePair("cp", "NO"));
            nameValuePairs.add(new BasicNameValuePair("CP", "NO"));
            nameValuePairs.add(new BasicNameValuePair("TXTInfo", ""));
            nameValuePairs.add(new BasicNameValuePair("o", String.valueOf(station1_id)));
            nameValuePairs.add(new BasicNameValuePair("d", String.valueOf(station2_id)));
            nameValuePairs.add(new BasicNameValuePair("df", String.valueOf(fulldate)));
            nameValuePairs.add(new BasicNameValuePair("ho", "00"));
            nameValuePairs.add(new BasicNameValuePair("hd", "26"));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            Log.d(TAG, "loadSchedule| before httpclient.execute");

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);

            Log.d(TAG, "loadSchedule| before getContent");
            StringBuilder result = inputStreamToString(response.getEntity().getContent());

            String parsed = null;

            //EscribirAFichero(result.toString());

            Log.d(TAG, "loadSchedule| before parser_html");
            parsed = parser_html(result.toString());

            //EscribirAFichero(parsed);

            Log.d(TAG, "loadSchedule| before SAXParserFactory");
            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser sp = spf.newSAXParser();

            XMLReader xr = sp.getXMLReader();
            HorarioHandler myExampleHandler = new HorarioHandler();
            xr.setContentHandler(myExampleHandler);

            InputStream is = new ByteArrayInputStream(parsed.getBytes());
            xr.parse(new InputSource(is));

            test = myExampleHandler.getParsedExampleDataSets();

            Message myMessage=new Message();
            myMessage.obj="SUCCESS";
            handler.sendMessage(myMessage);



        } catch (SAXException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            Log.d(TAG, "loadSchedule| SAXException");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            Log.d(TAG, "loadSchedule| UnsupportedEncodingException");
        } catch (ClientProtocolException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            Log.d(TAG, "loadSchedule| ClientProtocolException");
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            Log.d(TAG, "loadSchedule| IOException");
        } catch (ParserConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            Log.d(TAG, "loadSchedule| ParserConfigurationException");
        }
    }

    private String parser_html (String url_content)
    {
        Log.d(TAG, "parser_html entering");

        String st1  = "<table border=\"0\" width=\"100%\" cellpadding=\"1\" cellspacing=\"1\" align=\"center\">";
        String st2  = "<td align=center valign=\"middle\" colspan=\"5\">";
        String st3  = "<tr>";
        String st4  = "</tr>";
        String st5  = "<td class=color1 align=center>";
        String st6  = "<td class=color2 align=center>";
        String st7  = "<td class='color3' >";
        String st8  = "<td class=color1 align='center'>";
        String st9  = "</td>";
        String st10 = "Transbordo en";
        String st11 = "<td class='cabe' colspan=2 align=center>";
        String st12 = "<td class='esta' align=center>Salida</td>";
        String hs = "";
        String hl = "";
        String li = "";
        String ti = "";
        String ol = "";
        String hd = "";
        String dl = "";
        String resultado = "";
        int countador = 0;
        String contenido_url = "";
        String val = "";
        String trans = null;


        contenido_url = url_content.substring( url_content.indexOf(st1) +  st1.length()  );
        if (contenido_url.indexOf("</table>") != -1)
            contenido_url = contenido_url.substring(0, contenido_url.indexOf("</table>"));
        contenido_url = contenido_url.substring(contenido_url.indexOf(st2) + st2.length());

        val = contenido_url.substring(contenido_url.indexOf("<tr>") + st3.length());
        if (val.indexOf(st4) != -1)
            val = val.substring(0, val.indexOf(st4));

        countador = count(val, "<td" );

        resultado = resultado + "<HorarioList>\n\n";

        Log.d(TAG, "parser_html eval countador");

        if (countador == 5)
        {
//               CABECERA DE LA TABLA EN LA WEB DE RENFE:
//               - Hora de Salida
//               - Hora de Llegada
//               - Línea
//               - Tiempo

            Log.d(TAG, "parser_html inside countador 5");

            val = contenido_url.substring(contenido_url.indexOf(st3) + st3.length());
            val = val.substring(0, val.indexOf(st4));

            int i = 0;
            while (!val.matches(""))
            {
                if (i == 0)
                {
                    contenido_url = contenido_url.substring(contenido_url.indexOf(st4) + st4.length());
                    val = contenido_url.substring(contenido_url.indexOf(st3) + st3.length());
                    val = val.substring(0, val.indexOf(st4));
                    i++;
                    continue;
                }

                if(val.matches("Volver"))
                {
                    break;
                }

                resultado = resultado + "<Horario>\n";

                // Extraemos la hora de salida
                hs = val.substring(val.indexOf(st5) + st5.length());
                hs = hs.substring(0, hs.indexOf(st9));
                hs = stipHtml(hs);
                hs = hs.trim();

                // Extraemos la hora de llegada
                hl = val.substring(val.indexOf(st6) + st6.length());
                hl = hl.substring(0, hl.indexOf(st9));
                hl = stipHtml(hl);
                hl = hl.trim();

                // Extraemos la línea de salida
                li = val.substring(val.indexOf(st7) + st7.length());
                li = li.substring(0, li.indexOf(st9));
                li = stipHtml(li);
                li = li.trim();

                // Extraemos el tiempo de viaje
                ti = val.substring(val.indexOf(st8) + st8.length());
                ti = ti.substring(0, ti.indexOf(st9));
                ti = stipHtml(ti);
                ti = ti.trim();

                resultado = resultado + "<salida value=\"" + hs + "\" />\n";
                resultado = resultado + "<llegada value=\"" + hl + "\" />\n";
                resultado = resultado + "<tiempo value=\"" + ti + "\" />\n";
                resultado = resultado + "<lineasalida value=\"" + li + "\" />\n";
                resultado = resultado + "<transbordo value=\"\" />\n";
                resultado = resultado + "<transbordolinea value=\"\" />\n";

                contenido_url = contenido_url.substring(contenido_url.indexOf(st4) + st4.length());

                val = contenido_url.substring(contenido_url.indexOf(st3) + st3.length());
                val = val.substring(0, val.indexOf(st4) + 1);
                i++;

                resultado = resultado + "</Horario>\n\n";
            }
        }
        else if (countador == 6)
        {
//               CABECERA DE LA TABLA EN LA WEB DE RENFE:
//               - Línea
//               - Hora de salida en origen
//               - Transbordo en
//                 - Estacion
//                 - Llegada
//                 - Salida
//               - Hora de Llegada en destino
//               - Línea
//               - Tiempo de viaje

            Log.d(TAG, "parser_html inside countador 6");

            val = contenido_url.substring(contenido_url.indexOf(st3) + st3.length());
            val = val.substring(0, val.indexOf(st4));

            // Extraemos la estación de transbordo
            trans = contenido_url.substring(contenido_url.indexOf(st10) + st10.length() );
            trans = trans.substring(trans.indexOf(st11) + st11.length());
            trans = trans.substring(0, trans.indexOf(st9));
            trans = stipHtml(trans);
            trans = trans.trim();
            // TODO: utf8_encode trans

            Log.d(TAG, "parser_html| trans=|" + trans + "|" );

            contenido_url = contenido_url.substring(contenido_url.indexOf(st12) + st12.length());

            val = contenido_url.substring(contenido_url.indexOf(st3) + st3.length());
            val = val.substring(0, val.indexOf(st4));

            while(!val.matches(""))
            {
                if(val.matches("javascript"))
                {
                    Log.d(TAG, "parser_html| preg_match javascript");
                    break;
                }

                val = stipHtml(val);

                Log.d(TAG, "parser_html| val_STIPPED=|" + val + "|" );
//                EscribirAFichero("parser_html| val_STIPPED=|" + val + "|");

                String[] val_array = val.split(" ");

                if (val_array.length >= 0) ol = val_array[0].trim(); // Línea de salida
                if (val_array.length >= 7) hl = val_array[1].trim(); // Hora de salida
                if (val_array.length >= 7) hd = val_array[4].trim(); else hd = ""; // Hora de llegada
                if (val_array.length >= 7) dl = val_array[5].trim(); else dl = ""; // Línea de transbordo
                if (val_array.length >= 7) ti = val_array[6].trim(); else ti = ""; // Tiempo de viaje

                Log.d(TAG, "val_array.length|" + val_array.length + "|\n");
                Log.d(TAG, "ol|" + ol + "|\n");
                Log.d(TAG, "hl|" + hl + "|\n");
                Log.d(TAG, "hd|" + hd + "|\n");
                Log.d(TAG, "dl|" + dl + "|\n");
                Log.d(TAG, "ti|" + ti + "|\n");

//                EscribirAFichero("ol|" + ol + "|\n");
//                EscribirAFichero("hl|" + hl + "|\n");
//                EscribirAFichero("hd|" + hd + "|\n");
//                EscribirAFichero("dl|" + dl + "|\n");
//                EscribirAFichero("ti|" + ti + "|\n");


                if (hl.equals("") || hd.equals(""))
                {
                    //Log.d(TAG, "parser_html| inside hl || hd condition" );
                    contenido_url = contenido_url.substring(contenido_url.indexOf(st4) + st4.length());

                    val = contenido_url.substring(contenido_url.indexOf(st3) + st3.length());
                    if (-1 != val.indexOf(st4) && !val.equals(""))
                        val = val.substring(0, val.indexOf(st4));
                    continue;
                }

                resultado = resultado + "<Horario>\n";
                resultado = resultado + "<salida value=\"" + hl + "\" />\n";
                resultado = resultado + "<llegada value=\"" + hd + "\" />\n";
                resultado = resultado + "<tiempo value=\"" + ti + "\" />\n";
                resultado = resultado + "<lineasalida value=\"" + ol + "\" />\n";
                resultado = resultado + "<transbordo value=\"" + trans + "\" />\n";
                resultado = resultado + "<transbordolinea value=\"" + dl + "\" />\n";
                resultado = resultado + "</Horario>\n\n";

                contenido_url = contenido_url.substring(contenido_url.indexOf(st4) + st4.length());

                val = contenido_url.substring(contenido_url.indexOf(st3) + st3.length());
                if (-1 != val.indexOf(st4) && !val.equals(""))
                    val = val.substring(0, val.indexOf(st4));
                else
                    break;

                Log.d(TAG, "parser_html| val=|" + val + "|" );

            }

        }

        resultado = resultado + "</HorarioList>\n\n";
        Log.d(TAG, "parser_html exiting");
        return resultado;
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String loginmsg=(String)msg.obj;
            if(loginmsg.equals("SUCCESS")) {

                int i=0;

                for(ParsedHorarioDataSet v:test)
                {

                    TableRow tr = new TableRow(HorariosActivity.this);

                    if(i%2!=0)
                        tr.setBackgroundColor((Color.argb(0xFF, 0xD9, 0xE2, 0xF3)));

                    TextView a = new TextView(HorariosActivity.this);
                    a.setText(v.getHoraSalida());
                    a.setTextSize(15);
                    a.setTextColor((Color.argb(0xFF, 0, 0, 0)));
                    //TODO: original a.setPadding(3, 3, 5, 3);
                    a.setPadding(5, 3, 12, 3);
                    a.setGravity(Gravity.CENTER_HORIZONTAL);

                    TextView b = new TextView(HorariosActivity.this);
                    b.setText(v.getHoraLlegada());
                    b.setTextSize(15);
                    b.setTextColor((Color.argb(0xFF, 0, 0, 0)));
                    //TODO: original b.setPadding(3, 3, 3, 3);
                    b.setPadding(7, 3, 15, 3);
                    b.setGravity(Gravity.CENTER_HORIZONTAL);

                    TextView d = new TextView(HorariosActivity.this);
                    d.setText(v.getTiempo());
                    d.setTextSize(15);
                    d.setTextColor((Color.argb(0xFF, 0, 0, 0)));
                    //TODO: original d.setPadding(3, 3, 3, 3);
                    d.setPadding(23, 3, 15, 3);
                    d.setGravity(Gravity.CENTER_HORIZONTAL);

                    TextView e = new TextView(HorariosActivity.this);
                    e.setText(v.getLineaSalida());
                    e.setTextSize(15);
                    e.setTextColor((Color.argb(0xFF, 0, 0, 0)));
                    //TODO: original  e.setPadding(3, 3, 3, 3);
                    e.setPadding(20 , 3, 15, 3);
                    e.setGravity(Gravity.CENTER_HORIZONTAL);


                    if(!v.getTransbordo().equals("")){

                        char letra;
                        String texto="",nuevoTexto="", resto="";
                        //Borrando los espacios en blanco
                        texto=v.getTransbordo().toLowerCase();
                        //cogiendo un string a partir de la segunda letra
                        resto=texto.substring(1);
                        //cogiendo la primera letra en un char
                        letra=texto.charAt(0);
                        //poniendo dicha letra en mayusculas
                        letra = Character.toUpperCase(letra);
                        //concatenando todo
                        nuevoTexto=letra+resto + " (Línea " + v.getTransbordoLinea() + ")";

                        info_transbordo.setVisibility(0);
                        info_transbordo.setText("Transbordo en:\n" + nuevoTexto);
                    }
                    else{
                        info_transbordo.setHeight(1);
                    }

                    tr.addView(a);
                    tr.addView(b);
                    tr.addView(d);
                    tr.addView(e);

                    tl.addView(tr);

                    i++;

                }

                // No siempre es culpa de mi aplicacion
                if(i==0){
                    Toast.makeText(getApplicationContext(), "La web de Renfe no responde o no es posible el trayecto", Toast.LENGTH_SHORT).show();
                }


                removeDialog(0);
            }
        }
    };

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0: {
                dialog = new ProgressDialog(this);
                dialog.setMessage("Cargando...");
                dialog.setIndeterminate(true);
                dialog.setCancelable(true);
                return dialog;
            }
        }
        return null;
    }


}

