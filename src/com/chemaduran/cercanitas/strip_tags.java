/*
 * Cercanitas
 *
 * Chema Durán jgduran@gmail.com
 *
 */

package com.chemaduran.cercanitas;

import java.lang.reflect.Modifier;
import java.util.regex.Pattern;

public class strip_tags extends Modifier {

    private static Pattern p = Pattern
            .compile("</?\\p{Alpha}+ *( +\\p{Alpha}+ *=(\"(\\\\.|[\\x00-\\x21\\x23-\\x5b\\u005d-\\uffff])*\"|'(\\\\.|[\\x00-\\x26\\x28-\\x5b\\u005d-\\uffff])*'|[\\x00-\\x3d\\u003f-\\uffff]*) *)*>");

    //@Override
    protected Object execute(Object var, Object[] values) {
        return p.matcher(var.toString()).replaceAll("");
    }
}
